# SQL Analyzer - Specs

## SQL Standard

The latest SQL standard (2023) is available from [ISO Standard 9075](https://www.iso.org/search.html?q=9075). The most interesting document is the [SQL Foundation](https://www.iso.org/standard/76584.html).

## DB2

[DB2 for z/OS - SQL: The language of DB2](https://www.ibm.com/docs/en/db2-for-zos/13?topic=db2-sql)

## Informix

[IBM Informix 12.10 - SQL Programming](https://www.ibm.com/docs/en/informix-servers/12.10?topic=sql-programming)

## MariaDB / MySQL

* [MariaDB Server Knowledge Base (pdf)](https://mariadb.org/wp-content/uploads/2023/11/MariaDBServerKnowledgeBase.pdf) from the MariaDB.org website. This is the november 2023 version, check for the latest version.

## Oracle PL/SQL

* [Database PL/SQL 19c Language Reference](https://docs.oracle.com/en/database/oracle/oracle-database/19/lnpls/database-pl-sql-language-reference.pdf) from the Oracle Documentation website.

* [SQL 19c Language Reference](https://docs.oracle.com/en/database/oracle/oracle-database/19/sqlrf/sql-language-reference.pdf) from the Oracle Documentation website.

## PostgreSQL

[PostgreSQL 16.1 Documentation ](https://www.postgresql.org/files/documentation/pdf/16/postgresql-16-A4.pdf) from the PostgreSQL website. In particular Part III of this document covers SQL.

## SAP HANA

[SAP HANA SQL Reference for the SAP HANA Platform](https://help.sap.com/docs/SAP_HANA_PLATFORM/4fe29514fd584807ac9f2a04f6754767/b4b0eec1968f41a099c828a4a6c8ca0f.html)

## TransActSQL

* [Transact-SQL Reference v.16](https://learn.microsoft.com/en-us/sql/t-sql/language-reference?view=sql-server-ver16) from the Microsoft Documentation website.
